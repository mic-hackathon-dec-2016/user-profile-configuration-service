import os

import MySQLdb as mdb
import boto3
from flask import Flask, jsonify
from flask import abort
from flask import make_response
from flask import request

app = Flask(__name__)

queue_name = os.getenv('QUEUE_NAME')
print(queue_name)
aws_region = os.getenv('AWS_DEFAULT_REGION')
print(aws_region)
aws_access_id = os.getenv('AWS_ACCESS_KEY_ID')
print(aws_access_id)
aws_secret_key = os.getenv('AWS_SECRET_ACCESS_KEY')
print(aws_secret_key)
db_host = os.getenv('DB_HOST')
print(db_host)
db_user = os.getenv('DB_USER')
print(db_user)
db_password = os.getenv('DB_PASSWORD')
print(db_password)
db_name = os.getenv('DB_NAME')
print(db_name)


def create_table():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute(
            "CREATE TABLE IF NOT EXISTS USER_CONFIG(Id INT NOT NULL AUTO_INCREMENT, Name VARCHAR(100) NOT NULL, Subscription VARCHAR(300) NOT NULL, Frequency VARCHAR(3) NOT NULL, PRIMARY KEY (Id))")


def insert_values(name, subscription, frequency):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute(
            "INSERT INTO USER_CONFIG (Name, Subscription, Frequency) VALUES ('" + name + "','" + subscription + "','" + frequency + "')")
        return cur.lastrowid


def delete_values(id):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("DELETE FROM USER_CONFIG WHERE id =" + str(id))
        print("Data For User Deleted.")
        return cur.lastrowid


def update_values(id, frequency):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("UPDATE USER_CONFIG SET Frequency='" + frequency + "' WHERE id =" + str(id))
        print("Data For User Updated.")


def select_values(id):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("SELECT * from USER_CONFIG WHERE id =" + str(id))
        row = cur.fetchone()
        return row


def count_values():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("SELECT count(*) from USER_CONFIG")
        rows = cur.fetchone()
        return rows


def all_values():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("SELECT * from USER_CONFIG")
        rows = cur.fetchall()
        return rows


def get_user_details(id):
    row = select_values(id)
    if row is None:
        abort(404)
    return get_user_object(row)


@app.route('/')
def run():
    return 'User App is Up & Running. Give appropriate REST endpoints.\n', 200


@app.route('/ready', methods=['GET'])
def ready():
    create_table()
    return 'Table is Created and Ready to Use.\n', 200


@app.route('/health', methods=['GET'])
def health():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        return 'Able to Ping DB. Healthy Connection.\n', 200
    return 'Not able to Ping DB. Un-Healthy Connection.\n', 400


@app.route('/subscribe', methods=['POST'])
def create_user():
    if not request.json or not 'name' in request.json or not 'subscription' in request.json or not 'frequency' in request.json:
        abort(400)
    id = insert_values(request.json['name'], request.json.get('subscription', ""), request.json['frequency'])
    user = {
        'id': id,
        'name': request.json['name'],
        'subscription': request.json.get('subscription', ""),
        'frequency': request.json['frequency']
    }
    send_message(user)
    return jsonify({'user': user}), 201


def send_message(user):
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=queue_name)
    response = queue.send_message(MessageBody=str(user['id']),
                                  MessageAttributes={
                                      'Id': {
                                          'StringValue': str(user['id']),
                                          'DataType': 'String'
                                      },
                                      'Name': {
                                          'StringValue': user['name'],
                                          'DataType': 'String'
                                      },
                                      'Subscription': {
                                          'StringValue': user['subscription'],
                                          'DataType': 'String'
                                      },
                                      'Frequency': {
                                          'StringValue': str(user['frequency']),
                                          'DataType': 'String'
                                      }
                                  })
    print(response.get('MessageId'))


def delete_message(user):
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=queue_name)
    for message in queue.receive_messages(MessageAttributeNames=['Id']):
        if message.message_attributes is not None:
            user_id = message.message_attributes.get('Id').get('StringValue')
            if user_id == user['id']:
                print(message.message_attributes.get('Name'))
                message.delete()


def get_user_object(userdata):
    print("User Record:")
    print(userdata)
    user = {
        'id': str(userdata[0]),
        'name': userdata[1],
        'subscription': userdata[2],
        'frequency': userdata[3]
    }
    return user


@app.route('/users/<int:id>', methods=['GET'])
def get_user(id):
    user = get_user_details(id)
    return jsonify({'user': user})


@app.route('/users', methods=['GET'])
def get_users():
    users = []
    rows = all_values()
    print("All Records:")
    print(rows)
    for row in rows:
        user = get_user_object(row)
        users.append(user)
    return jsonify({'users': users})


@app.route('/users/<int:id>', methods=['PUT'])
def update_user(id):
    user = get_user_details(id)
    if not request.json:
        abort(400)
    update_values(id, request.json['frequency'])
    delete_message(user)
    send_message(user)
    return jsonify({'user': user})


@app.route('/users/<int:id>', methods=['DELETE'])
def delete_user(id):
    user = get_user_details(id)
    delete_values(id)
    delete_message(user)
    return jsonify({'result': True})


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'User Not found'}), 404)


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': 'Missing one of the required argument'}), 400)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
